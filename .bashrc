export CLICOLOR=1
export BASH_CONF="bashrc"

cd() {
	builtin cd "$@" && ls -a
}

# Simple command to ssh into local home pi server
alias pi='ssh phong-pi@192.168.1.43'

# List files by size
alias lt='du -sh * | sort -h'

# Show only mounted drives
alias mnt='mount | grep -E ^/dev | column -t'

# Find a command in grep history
alias gh='history|grep'

# List files by last modification
alias lm='ls -t -1'

# Count number of files in a directory
alias count='find . -type f | wc -l'

# Copy files with progress bar
alias cpv='rsync -ah --info=progress2'

# Instead of permanently deleting files. Move them to trash so that they are 
# recoverable in case of accidents
alias rm='mv --force -t ~/.Trash '

#TEST